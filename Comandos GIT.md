### Comando GIT

sudo git add . -> Agrega todos los cambios 

sudo git add <archivo> -> Agregar los cambios por archivo

sudo git restore <archivo> -> Descartar los cambios en el directorio de trabajo)

sudo git restore --staged <archivo> -> Restaurar los archivos ya agregado 

sudo git commit -m "Acá va el comentarios del cambio que se hizo" -> Crea un TAG o comentario para identificar el cambio realizado.

sudo git push -> Sube todos los cambios al repositorio de GitLab

sudo git pull  -> ese comando es para actualizar el repositorio para bajar los últimos cambios.
