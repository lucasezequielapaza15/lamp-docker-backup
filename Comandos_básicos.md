
# COMANDOS ÚTILES

* pwd - muestra el directorio en donde esta ubicado.

* ls - muestra todas las carpetas y archivos.

* ll - mustra todos el directorios dentro de una carpeta.

* cd - ingresar dicha carpeta en específico.

* nano "archivo" - editor de texto. 

* cat "archivo" - muestra el contenido del archivo en la consola.

* touch "archivo" - crear un archivo. 

* clear - limpia la consola.
 
* exit - salir de la consola.


# COMANDOS PARA CARPETAS

* mkdir "carpeta" - crear carpeta.

* rmdir "carpeta" - borrar carpeta si está vacía.

* rm -R "carpeta/archivo" - borrar carpeta si está llena.

* rm "archivo" - borrar los archivos.


# COMANDOS PARA COPIAR Y MOVER 

* cp "archivo" "directorio" - copiar archivo o carpeta.

* mv "archivo" "directorio" - mover archivo o carpeta.


# OTROS COMANDOS UTILES 

* df - mostrar el espacio del disco.

* man - manual de cada unos de los comandos.

* sudo - permiso del administrador (ubuntu).




